#pragma once

#ifndef NULLABLE_ASSERT
#define NULLABLE_ASSERT(expr, msg) void(0)
#endif

template <typename TValue>
struct Nullable
{
    using value_type = TValue;

    inline Nullable() noexcept
        : isInitialized_(false)
    {
    }

    inline explicit Nullable(value_type &&value)
        : isInitialized_(true)
    {
        new (&storage_) value_type(std::move(value));
    }

    inline explicit Nullable(const value_type &value)
        : isInitialized_(true)
    {
        new (&storage_) value_type(value);
    }

    inline ~Nullable() noexcept
    {
        auto ptr = get_ptr();
        if (ptr)
        {
            ptr->~value_type();
        }
        isInitialized_ = false;
    }

    inline Nullable(const Nullable &v)
        : isInitialized_(v.isInitialized_)
    {
        if (v.isInitialized_)
        {
            copy(&storage_, &v.storage_);
        }
    }

    inline Nullable(Nullable &&v) noexcept
        : isInitialized_(v.isInitialized_)
    {
        if (v.isInitialized_)
        {
            move(&storage_, &v.storage_);
        }
        v.isInitialized_ = false;
    }

     void swap(Nullable &v)
    {
        if (isInitialized_ && v.isInitialized_)
        {
            storage_type tmp;

            move(&tmp, &storage_);
            move(&storage_, &v.storage_);
            move(&v.storage_, &tmp);
        }
        else if (isInitialized_)
        {
            move(&v.storage_, &storage_);
            v.isInitialized_ = true;
            isInitialized_ = false;
        }
        else if (v.isInitialized_)
        {
            move(&storage_, &v.storage_);
            v.isInitialized_ = false;
            isInitialized_ = true;
        }
        else
        {
            // Do nothing
        }
    }

    inline Nullable &operator=(Nullable const &v)
    {
        if (this == std::addressof(v))
        {
            return *this;
        }

        Nullable<value_type> o(v);

        swap(o);

        return *this;
    }

    inline Nullable &operator=(Nullable &&v)
    {
        if (this == std::addressof(v))
        {
            return *this;
        }

        swap(v);

        return *this;
    }

    inline Nullable &operator=(value_type v)
    {
        return *this = Nullable(std::move(v));
    }

    inline void clear() noexcept
    {
        Nullable empty;
        swap(empty);
    }

    inline const value_type* get_ptr() const noexcept
    {
        if (isInitialized_)
        {
            return reinterpret_cast<const value_type*>(&storage_);
        }
        else
        {
            return nullptr;
        }
    }

    inline value_type* get_ptr() noexcept
    {
        if (isInitialized_)
        {
            return reinterpret_cast<value_type*>(&storage_);
        }
        else
        {
            return nullptr;
        }
    }

    inline const value_type& get() const noexcept
    {
        NULLABLE_ASSERT(isInitialized_, "not initialized");
        return *get_ptr();
    }

    inline value_type& get() noexcept
    {
        NULLABLE_ASSERT(isInitialized_, "not initialized");
        return *get_ptr();
    }

    inline bool HasValue() const noexcept
    {
        return isInitialized_;
    }

    inline explicit operator bool() const noexcept
    {
        return isInitialized_;
    }

    inline const value_type& operator*() const noexcept
    {
        return get();
    }

    inline value_type& operator*() noexcept
    {
        return get();
    }

    inline const value_type* operator->() const noexcept
    {
        return get_ptr();
    }

    inline value_type *operator->() noexcept
    {
        return get_ptr();
    }

  private:
     using storage_type = std::aligned_storage_t<sizeof(value_type), std::alignment_of<value_type>::value>;

    storage_type storage_;
    bool isInitialized_;

    inline static void move(storage_type *to, storage_type *from) noexcept
    {
        auto f = reinterpret_cast<value_type *>(from);
        new (to) value_type(std::move(*f));
        f->~value_type();
    }

    inline static void copy(storage_type *to, storage_type const *from)
    {
        auto f = reinterpret_cast<value_type const *>(from);
        new (to) value_type(*f);
    }
};