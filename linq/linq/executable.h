#pragma once
namespace linq
{

	template<class TFunc, class... Args>
	struct is_executable
	{
		using value = decltype(std::declval<TFunc>()( std::forward<Args>(std::declval<Args...>()...))
	};
}