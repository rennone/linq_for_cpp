#pragma once
#include <tuple>
namespace linq
{
	// https://gist.github.com/Angeart/ae347c4bdd5ed8b02385c411a723f9d0
	namespace lambda_detail
	{
		template<class Ret, class Cls, bool IsMutable, class... Args>
		struct types
		{
			static constexpr bool is_mutable = IsMutable;

			enum { arity = sizeof...(Args) };

			using return_type = Ret;

			template<size_t i>
			using arg_type = typename std::tuple_element<i, std::tuple<Args...>>::type;
		};
	}

	template<class Ld>
	struct lambda_type
		: lambda_type<decltype(&Ld::operator())>
	{};

	template<class Ret, class Cls, class... Args>
	struct lambda_type<Ret(Cls::*)(Args...)>
		: lambda_detail::types<Ret, Cls, true, Args...>
	{};

	template<class Ret, class Cls, class... Args>
	struct lambda_type<Ret(Cls::*)(Args...) const>
		: lambda_detail::types<Ret, Cls, false, Args...>
	{};

	template<typename TLambda, int N>
	using lambda_arg_type_t = typename lambda_type<TLambda>:: template arg_type<N>;

	template<typename TLambda>
	using lambda_ret_type_t = typename lambda_type<TLambda>::return_type;
}