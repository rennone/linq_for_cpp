#pragma once
#include <type_traits>
#include "lambda_trait.h"
namespace linq
{
	template<class TSource>
	using val_type_t = decltype(std::declval<TSource>().Current());

	// constと参照を外した型
	template<class T>
	using raw_type_t = std::remove_cv_t<std::remove_reference_t<T>>;

	// イテレータの中身の型を取得
	template<class TIter>
	using iter_val_t = decltype(*std::declval<TIter>());

	template<class TSource, class TPredicate>
	using predicate_value_t = decltype(std::declval<TPredicate>()(std::declval<val_type_t<TSource>>()));

	// TSource::value_type
	template<class TSource>
	struct lazy_eval_value
	{
		using source_type = raw_type_t<TSource>;
		using type = raw_type_t<val_type_t<source_type>>;
		lazy_eval_value(const type& v)
			:v_(v)
		{ }

		operator type() {
			return v_;
		}

		type v_;
	};

	template<class T>
	auto ThrowReturn()
	{
		return (raw_type_t<T>*)nullptr;
	}


}