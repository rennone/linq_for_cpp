#pragma once
namespace linq
{
	template<class T>
	class IEnumerable
	{
	public:
		virtual ~IEnumerable() = default;
		virtual T Current() const = 0;
	};
}