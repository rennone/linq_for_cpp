﻿#pragma once
#include <algorithm>
#include <vector>
#include <type_traits>
#include <iterator>
#include <tuple>
#include <array>
#include "nullable.hpp"
#include "linq_util.h"

#ifndef LINQ_ASSERT
#define LINQ_ASSERT(expr, msg) ((void)0)
#endif
#ifndef LINQ_THROW_RETURN
//#define LINQ_THROW_RETURN(type_name, msg) return *linq::ThrowReturn<type_name>()
#define LINQ_THROW_RETURN(type_name, msg) throw std::runtime_error(msg)
#endif

namespace linq
{
	using size_type = std::size_t;

	using s32 = int;


	template<int N>
	struct Num;

	//namespace _impl
	//{
		static constexpr size_type invalid_size = (size_type)(-1);


		template<class TPredicate, class TValue>
		struct is_executable
		{
		private:
			//static auto check(...) -> std::false_type;
			template<class TPred>
			static auto check(TPred&& predicate) -> decltype(predicate(std::declval<TValue>()), std::true_type());
		public:
			using type = decltype(check(std::declval<TPredicate>()));
			static constexpr bool value = type();
		};

		template<typename TSource>
		struct EnumerableIterator : public std::iterator<std::forward_iterator_tag, raw_type_t<val_type_t<TSource>>>
		{
			using value_type  = val_type_t<TSource>;

			EnumerableIterator()
				: isEnd_(true)
			{}

			EnumerableIterator(TSource source)
				:source_(source), count_(0)
			{
				isEnd_ = !source_->Next();
			}

			EnumerableIterator(const EnumerableIterator& other)
				:source_(other.source_), isEnd_(other.isEnd_), count_(other.count_)
			{}

			value_type operator*()
			{
				LINQ_ASSERT(!isEnd_, "end of iterator");
				if (!isEnd_)
					return source_->Current();

				LINQ_THROW_RETURN(value_type, "end of iterator");
			}

			// 前置記法
			auto& operator++()
			{
				if (!isEnd_)
				{
					if (source_->Next())
					{
						++count_;
					}
					else
					{
						isEnd_ = true;
					}
				}
				return *this;
			}

			// 後置記法
			auto operator++(int)
			{
				auto ret = EnumerableIterator(*this);
				operator++();
				return ret;
			}

			bool operator==(const EnumerableIterator& other)
			{
				if (isEnd_ != other.isEnd_)
					return false;

				// both end or same count
				return isEnd_ || count_ == other.count_;
			}

			bool operator!=(const EnumerableIterator& other)
			{
				return !(operator==(other));
			}

		private:
			Nullable<TSource> source_;
			bool isEnd_ = false;
			int count_ = 0;
		};

		template<class TValue>
		struct EmptyEnumerable
		{
			using this_type = EmptyEnumerable<TValue>;
			using value_type = TValue;
			using return_type = TValue;

			inline return_type Current() const
			{
				LINQ_THROW_RETURN(return_type, "empty");
			}

			inline bool Next() noexcept
			{
				return false;
			}
		};

		// Iterator型をそのまま返すEnumerable
		template<class TIter>
		struct IterEnumerable
		{
			using iter_type = raw_type_t<TIter>;
			using value_type = raw_type_t<TIter>;
			using this_type = IterEnumerable<TIter>;

			inline IterEnumerable(iter_type begin, iter_type end) noexcept
				: current_(begin)
				, next_(current_)
				, end_(end)
			{ }

			inline IterEnumerable(const this_type& v) noexcept
				: current_(v.current_)
				, next_(v.next_)
				, end_(v.end_)
			{ }

			inline value_type Current() const
			{
				LINQ_ASSERT(current_ != next_, "Call Current() before Next()");
				LINQ_ASSERT(current_ != end_, "Call Current() for end iterator");
				return current_;
			}

			inline bool Next() noexcept
			{
				if (next_ == end_)
				{
					return false;
				}

				current_ = next_;
				++next_;
				return true;
			}

		private:
			iter_type current_;
			iter_type next_;
			iter_type end_;
		};

		// Iteratorの中身を返すEnumerable
		template <typename TIter>
		struct FromEnumerable
		{
			using iter_type = raw_type_t<TIter>;
			using value_type = iter_val_t<iter_type>;
			using this_type = FromEnumerable<iter_type>;

			inline FromEnumerable(iter_type begin, iter_type end) noexcept
				: current_(begin)
				, next_(current_)
				, end_(end)
			{ }

			inline FromEnumerable(const this_type& v) noexcept
				: current_(v.current_)
				, next_(v.next_)
				, end_(v.end_)
			{ }

			inline auto Current() const -> decltype(auto)
			{
				LINQ_ASSERT(current_ != next_, "Call Current() before Next()");
				LINQ_ASSERT(current_ != end_, "Call Current() for end iterator");
				return *current_;
			}

			inline bool Next() noexcept
			{
				if (next_ == end_)
				{
					return false;
				}

				current_ = next_;
				++next_;
				return true;
			}

		private:
			iter_type current_;
			iter_type next_;
			iter_type end_;
		};

		struct RangeEnumerable
		{
			using this_type = RangeEnumerable;
			using value_type = s32;
			
			inline RangeEnumerable(	s32 start, s32 count) noexcept
				: current_(start - 1)
				, end_(start + count - 1)
			{}

			inline RangeEnumerable(const this_type &v) noexcept
				: current_(v.current_)
				, end_(v.end_)
			{}

			inline value_type Current() const
			{
				return current_;
			}

			inline bool Next() noexcept
			{
				if (current_ >= end_)
				{
					return false;
				}

				++current_;
				return true;
			}

		private:
			value_type current_;
			value_type end_;
		};


		template< class TSource, class TCollectionSelector>
		struct SelectManyEnumerable
		{
		public:
			using this_type = SelectManyEnumerable<TSource, TCollectionSelector>;
			using source_type = raw_type_t<TSource>;
			using elem_enumerable_type = decltype(std::declval<TCollectionSelector>()(std::declval<TSource>().Current()));
			using value_type = raw_type_t<decltype(std::declval<elem_enumerable_type>().Current())>;

			inline SelectManyEnumerable(source_type source, TCollectionSelector selector) noexcept
				: source_(source)
				, selector_(selector)
			{ }

			inline SelectManyEnumerable(const this_type &v)
				: source_(v.source_)
				, selector_(v.selector_)
				, current_(v.current_)				
			{ }

			inline value_type Current() const
			{
				return (*current_).Current();
			}

			inline bool Next()
			{
				while (current_.HasValue() == false || (*current_).Next() == false)
				{
					if (source_.Next() == false)
						return false;

					current_ = selector_(source_.Current());
				}

				return true;
			}

		private:
			source_type source_;
			TCollectionSelector selector_;
			Nullable<elem_enumerable_type> current_;
		};


		template <typename TSource, typename TPredicate>
		struct SelectEnumerable
		{
			using source_type = raw_type_t<TSource>;
			using predicate_type = raw_type_t<TPredicate>;
			using this_type = SelectEnumerable<TSource, TPredicate>;
			using value_type = predicate_value_t<source_type, predicate_type>;

			inline SelectEnumerable(source_type source, predicate_type predicate) noexcept
				: source_(source)
				, predicate_(predicate)
			{ }

			inline SelectEnumerable(const this_type &v)
				: source_(v.source_)
				, predicate_(v.predicate_)
			{ }
			
			inline value_type Current() const
			{
				return predicate_(source_.Current());
			}

			inline bool Next()
			{
				return source_.Next();
			}
		private:
			source_type source_;
			predicate_type predicate_;
		};

		template <typename TSource, typename TPredicate>
		struct CachedSelectEnumerable
		{
			using source_type    = raw_type_t<TSource>;
			using predicate_type = raw_type_t<TPredicate>;
			using this_type      = CachedSelectEnumerable<TSource, TPredicate>;
			using value_type     = raw_type_t<predicate_value_t<source_type, predicate_type>>;
			

			inline CachedSelectEnumerable(source_type source, predicate_type predicate) noexcept
				: source_(source)
				, predicate_(predicate)
			{ }

			inline CachedSelectEnumerable(const this_type &v)
				: source_(v.source_)
				, predicate_(v.predicate_)
				, cacheValue_(v.cacheValue_)
			{}

			inline value_type Current() const
			{
				LINQ_ASSERT(cacheValue_, "call Current before Next");
				return *cacheValue_;
			}

			inline bool Next()
			{
				if (source_.Next())
				{
					cacheValue_ = predicate_(source_.Current());
					return true;
				}

				cacheValue_.clear();
				return false;
			}

		private:
			source_type source_;
			predicate_type predicate_;
			Nullable<value_type> cacheValue_;
		};

		template<typename TSource>
		struct TakeEnumerable
		{
			using source_type = raw_type_t < TSource >;
			using value_type = val_type_t<TSource>;
			using this_type = TakeEnumerable<TSource>;

			inline TakeEnumerable(source_type source, int skip_count)
				: source_(source)
				, takeCount_(skip_count)
				, nowCount_(0)
			{ }

			inline TakeEnumerable(const this_type& other)
				: source_(other.source_)
				, takeCount_(other.takeCount_)
				, nowCount_(other.nowCount_)
			{ }

			inline value_type Current() const
			{
				return source_.Current();
			}

			inline bool Next()
			{
				if (nowCount_ >= takeCount_)
					return false;

				++nowCount_;
				return source_.Next();
			}

		private:
			source_type source_;
			int takeCount_ = 0;
			int nowCount_  = 0;
		};

		template<typename TSource, class TPredicate>
		struct TakeUntilEnumerable
		{
			using this_type = TakeEnumerable<TSource>;
			using value_type = val_type_t<TSource>;
			using source_type = raw_type_t < TSource >;
			using predicate_type = raw_type_t<TPredicate>;

			inline TakeUntilEnumerable(source_type source, predicate_type predicate)
				: source_(source)
				, predicate_(predicate)
			{ }

			inline TakeUntilEnumerable(const this_type& other)
				: source_(other.source_)
				, predicate_(other.predicate_)
			{ }

			inline value_type Current() const
			{
				return source_.Current();
			}

			inline bool Next()
			{
				if (isEnd_)
					return false;

				// 要素が存在して, かつ条件を満たす場合はtrue
				if (source_.Next() && predicate_(source_.Current()))
					return true;

				// そうじゃない場合は終了
				isEnd_ = true;
				return false;
			}

		private:
			source_type source_;
			predicate_type predicate_;
			bool isEnd_ = false;
		};

		template<typename TSource>
		struct SkipEnumerable
		{
			using this_type = SkipEnumerable<TSource>;
			using source_type = raw_type_t<TSource>;
			using value_type = val_type_t<TSource>;		

			inline SkipEnumerable(source_type source, int skip_count)
				: source_(source)
				, skipCount_(skip_count)
				, nowCount_(0)
			{}

			inline SkipEnumerable(const this_type& other )
				: source_(other.source_)
				, skipCount_(other.skipCount_)
				, nowCount_(other.nowCount_)
			{ }

			inline value_type Current() const
			{
				LINQ_ASSERT(nowCount_ > 0, "Next not called");
				return source_.Current();
			}

			inline bool Next()
			{
				// 未初期化の場合
				if (nowCount_ <= 0)
				{
					bool has_next = true;
					nowCount_ = 0;
					while (nowCount_++ < skipCount_)
					{
						LINQ_ASSERT(has_next, "linq skip : out of range");
						has_next = source_.Next();
					}
				}
				return source_.Next();
			}
			
		private:
			source_type source_;
			int skipCount_ = 0;
			int nowCount_ = -1;
		};

		template<typename TSource, typename TPredicate>
		struct SkipWhileEnumerable
		{
			using this_type = SkipWhileEnumerable<TSource, TPredicate>;
			using source_type    = raw_type_t<TSource>;
			using predicate_type = raw_type_t<TPredicate>;

			using value_type = val_type_t<TSource>;			

			inline SkipWhileEnumerable(source_type source, predicate_type predicate)
				: source_(source)
				, predicate_(predicate)
				, stopSkip_(false)
			{	}

			inline SkipWhileEnumerable(const this_type& other )
				: source_(other.source_)
				, predicate_(other.predicate_)
				, stopSkip_(other.stopSkip_)
			{ }

			inline value_type Current() const
			{
				LINQ_ASSERT(stopSkip_, "Next not called");
				return source_.Current();
			}

			inline bool Next()
			{
				if(!stopSkip_)
				{
					stopSkip_ = true;
					while (source_.Next())
					{
						// 要素がなくなるか条件を満たさなくなるまでループする
						// 条件の方の場合はtrue
						if (predicate_(source_.Current()) == false)
							return true;
					}
					// 要素がなくなった場合はfalse
					return false;
				}
				
				return source_.Next();
			}

		private:
			source_type source_;
			predicate_type predicate_;
			bool stopSkip_ = false;
		};

		template <typename TSource, typename TPredicate>
		struct WhereEnumerable
		{
			using this_type = WhereEnumerable<TSource, TPredicate>;
			using source_type = raw_type_t<TSource>;
			using predicate_type = raw_type_t<TPredicate>;
			using value_type =val_type_t<TSource>;

			inline WhereEnumerable(source_type source, predicate_type predicate) noexcept
				: source_(source)
				, predicate_(predicate)
			{}

			inline WhereEnumerable(const  this_type& v)
				: source_(v.source_)
				, predicate_(v.predicate_)
			{}

			inline value_type Current() const
			{
				return source_.Current();
			}

			inline bool Next()
			{
				while (source_.Next())
				{
					if (predicate_(source_.Current()))
					{
						return true;
					}
				}
				return false;
			}
		private:
			source_type source_;
			predicate_type predicate_;
		};

		template <typename TValue>
		struct RepeatEnumerable
		{
			using this_type = RepeatEnumerable<TValue>;
			using value_type = TValue;

			inline RepeatEnumerable(value_type element, size_type count) noexcept
				: value_(std::move(element))
				, remain_(count)
				, count_(count)
			{
			}

			inline RepeatEnumerable(const this_type &v) noexcept
				: value_(v.value_)
				, remain_(v.remain_)
				, count_(v.count_)
			{
			}

			inline value_type Current() const
			{
				return value_;
			}

			inline bool Next() noexcept
			{
				if (remain_ == 0U)
				{
					return false;
				}

				--remain_;

				return true;
			}

		private:
			TValue value_;
			size_type remain_;
			size_type count_;
		};

		template<class TSource1, class TSource2>
		struct ConcatEnumerable
		{
			using this_type = ConcatEnumerable<TSource1, TSource2>;
			using source1_type = raw_type_t<TSource1>;
			using source2_type = raw_type_t<TSource2>;
			using value_type = val_type_t<source1_type>;

			ConcatEnumerable(source1_type source1, source2_type source2)
				: source1_(source1)
				, source2_(source2)
				, useSource2_(false)
			{}

			ConcatEnumerable(const this_type& other)
				: source1_(other.source1_)
				, source2_(other.source2_)
				, useSource2_(other.useSource2_)
			{}
			
			value_type Current() const {
				if (useSource2_)
					return source2_.Current();

				return source1_.Current();
			}

			bool Next() {
				if (useSource2_)
					return source2_.Next();

				if (source1_.Next())
					return true;

				useSource2_ = true;
				return source2_.Next();
			}

		private:
			source1_type source1_;
			source2_type source2_;
			bool useSource2_;
		};

		template<class T, bool is_reference = std::is_reference<T>::value>
		struct ref_to_ptr;

		template<class T>
		struct ref_to_ptr<T, false>
		{
			using type = raw_type_t<T>;

			static constexpr T revert(type v) {
				return v;
			}

			static constexpr type convert(T v) {
				return v;
			}
		};

		template<class T>
		struct ref_to_ptr<T, true>
		{
			using type = std::remove_reference_t<T>*;

			static constexpr T revert(type v) {
				return *v;
			}

			static constexpr type convert(T v) {
				return &v;
			}
		};

		template<class T>
		using ref_to_ptr_t = typename ref_to_ptr<T>::type;

		template <class TSelf, class TSource, int N>
		struct ASortEnumerable
		{
			using this_type = ASortEnumerable<TSelf, TSource, N>;
			using source_type  = raw_type_t<TSource>;
			using value_type    = val_type_t<TSource>;
			using element_type = ref_to_ptr_t<value_type>;
			using converter = ref_to_ptr<value_type>;

			inline ASortEnumerable(source_type source, bool assert_buffer_over = false) noexcept
				: source_(source)
				, current_(invalid_size)
				, assertBufferOver_(assert_buffer_over)
			{}

			inline ASortEnumerable(const this_type& v)
				: source_(v.source_)
				, current_(v.current_)
				, sortedValues_(v.sortedValues_)
				, assertBufferOver_(v.assertBufferOver_)
			{}		

			inline bool forwarding_Next()
			{
				return source_.Next();
			}

			inline value_type Current() const
			{
				return converter::revert(sortedValues_[current_]);
			}

			bool Next()
			{
				if (current_ == invalid_size)
				{
					Sort();
					current_ = 0U;
					return size_ > 0;
				}

				if (current_ < size_)
				{
					++current_;
				}

				return current_ < size_;
			}

		private:
			void Sort()
			{
				size_ = 0;
				while (source_.Next())
				{
					auto&& v = converter::convert(source_.Current());
					auto en = sortedValues_.begin() + size_;

					auto it = std::lower_bound(sortedValues_.begin(), en, v, [this](const element_type&l, const element_type&r)
					{
						return ((TSelf*)this)->Compare( converter::revert(l), converter::revert(r));
					});

					if (it != en || size_ < sortedValues_.size())
					{
						// TODO
						int n = (int)std::distance(sortedValues_.begin(), it);

						for (int i = (int)std::min(sortedValues_.size() - 1, size_); i > n; --i)
						{
							sortedValues_[i] = sortedValues_[i - 1];
						}

						sortedValues_[n] = v;
					}

					size_ = std::min(sortedValues_.size(), size_ + 1);
				}
			}
		private:
			source_type source_;
			size_type current_;
			size_type size_;
			std::array<element_type, N> sortedValues_;
			bool assertBufferOver_ = false;

		};

		template <typename TSource, typename TPredicate, int N>
		struct OrderByEnumerable : public ASortEnumerable<OrderByEnumerable<TSource, TPredicate, N>, TSource, N>
		{
			using base_type = ASortEnumerable<OrderByEnumerable<TSource, TPredicate, N>, TSource, N>;
			using this_type = OrderByEnumerable<TSource, TPredicate, N>;
			using source_type = raw_type_t<TSource>;
			using predicate_type = raw_type_t<TPredicate>;

			using value_type =val_type_t<TSource>;
			
			predicate_type predicate_;
			bool sortAscending_;

			inline OrderByEnumerable(source_type source, predicate_type predicate, bool sort_ascending, bool assert_buffer_over = false) noexcept
				: base_type(source, assert_buffer_over)
				, predicate_(std::move(predicate))
				, sortAscending_(sort_ascending)
			{
				//static_assert(!std::is_convertible<source_type, ASortEnumerable>::value, "orderby may not follow orderby or thenby");
			}

			inline OrderByEnumerable(const OrderByEnumerable &v)
				: base_type(v)
				, predicate_(v.predicate_)
				, sortAscending_(v.sortAscending_)
			{
			}

			inline OrderByEnumerable(OrderByEnumerable &&v) noexcept
				: base_type(v)
				, predicate_(std::move(v.predicate_))
				, sortAscending_(std::move(v.sortAscending_))
			{
			}

			inline bool Compare(const value_type  &lhs, const value_type  &rhs) const
			{
				if (sortAscending_)
				{
					return predicate_(lhs) < predicate_(rhs);
				}
				else
				{
					return predicate_(rhs) < predicate_(lhs);
				}
			}
		};

		// -------------------------------
		// Builder �n
		// -------------------------------

		template <typename TSource>
		struct ToVectorBuilder
		{
			using this_type = ToVectorBuilder<TSource>;

			inline explicit ToVectorBuilder(size_type capacity = 16U) noexcept
				: capacity(capacity)
			{
			}

			inline ToVectorBuilder(const ToVectorBuilder &v) noexcept
				: capacity(v.capacity)
			{
			}

			inline ToVectorBuilder(ToVectorBuilder &&v) noexcept
				: capacity(std::move(v.capacity))
			{
			}

			inline auto Build(TSource source) const
			{
				std::vector<typename TSource::value_type> result;
				result.reserve(capacity);

				while (source.Next())
				{
					result.push_back(source.Current());
				}

				return result;
			}

		private:
			size_type capacity;
		};

		template <class TSource>
		struct ToArrayBuilder
		{
			using this_type = ToArrayBuilder<TSource>;
			using value_type = val_type_t<TSource>;
			using source_type = raw_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			template <int N>
			inline auto Build(source_type source, raw_value_type(&ret)[N])
			{
				int i = 0;
				while (source.Next() && i < N)
				{
					ret[i++] = source.Current();
				}

				return i;
			}

			template<int N>
			inline auto Build(source_type source)
			{
				using ret_array = std::array <raw_value_type, N>;
				std::pair< ret_array, int> ret;
				ret_array& ary = std::get<0>(ret);
				int& num = std::get<1>(ret);

				num = 0;
				while (source.Next() && num < N)
				{
					ary[num++] = source.Current();
				}
				return ret;
			}
		};

		template<typename TSource>
		struct AnyBuilder
		{
			bool Build(TSource source)
			{
				return source.Next();
			}

			template<class TPredicate>
			bool Build(TSource source, TPredicate predicate)
			{
				while (source.Next())
				{
					if (predicate(source.Current()))
						return true;
				}

				return false;
			}
		};

		template<typename TSource>
		struct AllBuilder
		{
			template<class TPredicate>
			bool Build(TSource source, TPredicate&& predicate)
			{
				while (source.Next())
				{
					if (!predicate(source.Current()))
						return false;
				}

				return true;
			}
		};

		template<typename TSource>
		struct CountBuilder
		{
			s32 Build(TSource source)
			{
				s32 ret = 0;
				while (source.Next())
					ret += 1;

				return ret;
			}

			template<class TPredicate>
			s32 Build(TSource source, TPredicate&& predicate)
			{
				s32 ret = 0;
				while (source.Next())
				{
					if(predicate(source.Current()))
						ret += 1;
				}
				return ret;
			}
		};

		template<class TSource>
		struct SumBuilder
		{
			using source_type = raw_type_t<TSource>;
			using value_type = val_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			// Averageで処理を共通化するためにcountも返している
			auto Build(source_type source, s32& count) 
			{
				count = 0;
				raw_value_type ret = raw_value_type();
				while (source.Next()) {
					ret = ret + source.Current();
					count++;
				}
				return ret;
			}

			// Averageで処理を共通化するためにcountも返している
			template<class TSelector>
			auto Build(source_type source, TSelector&& selector, s32& count)
			{
				using predicate_type = raw_type_t<TSelector>;
				using ret_type = predicate_value_t<source_type, predicate_type >;

				ret_type ret = ret_type();
				count = 0;
				while (source.Next()) {
					ret = ret + selector(source.Current());
					count++;
				}

				return ret;
			}
		};

		template<class TSource>
		struct IndexOfBuilder
		{
			using this_type = IndexOfBuilder<TSource>;
			using value_type = val_type_t<TSource>;
			using source_type = raw_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			template<class TPredicate>
			inline auto Build(source_type source, TPredicate&& predicate)
			{
				int ret = 0;
				while (source.Next()) {
					if (predicate(source.Current()))
						return ret;
					ret++;
				}

				return -1;
			}
		};

		template<typename TSource>
		struct ElementAtBuilder
		{
			using this_type = ElementAtBuilder<TSource>;
			using value_type =val_type_t<TSource>;
			using source_type = raw_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			inline auto Build(source_type source, int index)
			{
				if (index < 0) {
					LINQ_THROW_RETURN(value_type, "index out of range");
				}

				for (auto i = 0; i <= index; ++i) 
				{
					source.Next();
				}
				return source.Current();
			}

			inline auto BuildOrDefault(source_type source, int index, const raw_value_type& seed)
			{
				if (index < 0)
					return seed;

				// 最初のNextの分1回多く回す
				for (auto i = 0; i <= index; ++i)
				{
					if (!source.Next())
						return seed;
				}

				return source.Current();
			}

			inline auto BuildOrDefault(source_type&& source, int index) 
			{
				return BuildOrDefault(source, index, raw_type_t<value_type>());
			}

			inline bool TryBuild(TSource source, int index, raw_value_type& out)
			{
				if (index < 0)
					return false;

				// 最初のNextの分1回多く回す
				for (auto i = 0; i <= index; ++i)
				{
					if (!source.Next())
						return false;
				}

				out = source.Current();
				return true;
			}
		};

		template<typename TSource>
		struct FirstBuilder
		{
			using this_type = FirstBuilder<TSource>;
			using value_type =val_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			inline auto Build(TSource source) -> decltype(auto)
			{
				if (source.Next())
					return source.Current();

				LINQ_THROW_RETURN(value_type, "no element");
			}

			inline auto BuildOrDefault(TSource source, const raw_value_type& seed)
			{
				if (source.Next())
					return source.Current();

				return seed;
			}

			inline bool TryBuild(TSource source, raw_value_type& out_ret)
			{
				if (source.Next())
				{
					out_ret = source.Current();
					return true;
				}

				return false;
			}
		};

		template<typename TSource>
		struct LastBuilder
		{
			using this_type = LastBuilder<TSource>;
			using value_type =val_type_t<TSource>;
			using raw_value_type = raw_type_t<value_type>;

			inline auto Build(TSource source)-> decltype(auto)
			{
				if (source.Next())
				{
					using c = ref_to_ptr<value_type>;
					using elem_type = ref_to_ptr_t<value_type>;
					elem_type ret = c::convert(source.Current());
					while (source.Next())
						ret = c::convert(source.Current());

					return c::revert(ret);
				}

				LINQ_THROW_RETURN(value_type, "no element");
			}

			inline auto BuildOrDefault(TSource source, const raw_value_type& seed)
			{
				if (source.Next())
				{
					using c = ref_to_ptr<value_type>;
					using elem_type = ref_to_ptr_t<value_type>;
					elem_type ret = c::convert(source.Current());
					while (source.Next())
						ret = c::convert(source.Current());

					return c::revert(ret);
				}

				return seed;
			}

			inline bool TryBuild(TSource source, raw_value_type& out_ret)
			{
				using c = ref_to_ptr<value_type>;
				using elem_type = ref_to_ptr_t<value_type>;

				if (source.Next())
				{
					elem_type ret = c::convert(source.Current());
					while (source.Next())
						ret = c::convert(source.Current());

					out_ret = c::revert(ret);
					return true;
				}
				return false;
			}
		};

		template<typename TSource>
		struct ComparerBuilder
		{
			using this_type   = ComparerBuilder<TSource>;
			using source_type = raw_type_t<TSource>;
			using value_type  = val_type_t<TSource>;
			using raw_value_type   = raw_type_t<value_type>;

			template<typename TComparer, typename TSelector>
			inline bool TryBuild(source_type source, TComparer&& comparer, TSelector&& selector, raw_value_type& ret )
			{
				if (!source.Next())
					return false;

				using e = ref_to_ptr<value_type>;
				using e_t = ref_to_ptr_t<value_type>;
				// 参照で受け取ると, 比較時に変更が走るたびに元のデータが変わってしまうのでそれ用
				e_t out = e::convert(source.Current());
				auto&& min = selector(e::revert(out));
				while (source.Next())
				{
					e_t e = e::convert(source.Current());
					auto&& v = selector(e::revert(e));
					if (comparer(v, min))
					{
						min = v;
						out = e;
					}
				}
				ret = e::revert(out);
				return true;
			}

			template<class TComparer, class TSelector>
			inline auto Build(source_type source, TComparer&& comparer, TSelector&& selector) -> decltype(auto)
			{
				if (!source.Next()) {
					LINQ_THROW_RETURN(value_type, "no element");
				}

				using e   = ref_to_ptr<value_type>;
				using e_t = ref_to_ptr_t<value_type>;
				// 参照で受け取ると, 比較時に変更が走るたびに元のデータが変わってしまうのでそれ用
				e_t out = e::convert(source.Current());
				auto&& min = selector(e::revert(out));
				while (source.Next())
				{
					e_t e = e::convert(source.Current());
					auto&& v = selector( e::revert(e));
					if (comparer(v, min))
					{
						min = v;
						out = e;
					}
				}
				return e::revert(out);
			}
		};

		template<class TSource>
		struct AggregateBuilder
		{
			using this_type = ComparerBuilder<TSource>;
			using source_type = raw_type_t<TSource>;

			template<class TAccumulator, class TValue>
			inline auto Build(source_type source, TAccumulator&& accumulator, const TValue& init_value)
			{
				using raw_type = raw_type_t<decltype(init_value)>;
				raw_type ret = init_value;
				while (source.Next())
				{
					ret = accumulator(ret, source.Current());
				}
				return ret;
			}
		};

	//}
} // namespace linq