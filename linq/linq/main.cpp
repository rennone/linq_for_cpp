#include "linq.hpp"

#include <iostream>
#include <assert.h>
#include <list>
//#define assert(expression, ...) 
using namespace std;

class NonCopyClass
{
	NonCopyClass(const NonCopyClass&) = delete;
	NonCopyClass& operator=(const NonCopyClass&) = delete;
};

struct NonCopyStruct
{
	NonCopyStruct() = default;
	NonCopyStruct(const NonCopyStruct&) = delete;
	NonCopyStruct& operator=(const NonCopyStruct&) = delete;
};

struct Vec2
{
	static bool show_print;

	static void print(const char* label)
	{
		if(show_print)
			printf("[Vec2] %*s:", 20, label);
	}

	Vec2(int x = 0, int y = 0)
	{
		this->x = x;
		this->y = y;
	}

	Vec2(const Vec2& other)
	{
		x = other.x;
		y = other.y;
		if (show_print) 
		{
			print("copy construcor");
			cout << *this << endl;
		}
	}

	Vec2& operator=(const Vec2& other) 
	{
		x = other.x;
		y = other.y;
		if (show_print)
		{
			print("asign");
			cout << *this << endl;
		}
		return *this;
	}

	void set(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

	void set(int v) {
		this->x = this->y = v;
	}

	bool operator==(const Vec2& other) const {
		return x == other.x && y == other.y;
	}

	bool operator <(const Vec2& other) const {
		return x * y < other.x * other.y;
	}

	bool operator >(const Vec2& other) const {
		return x * y > other.x * other.y;
	}

	//Vec2(const Vec2& other) = delete;
	//Vec2& operator=(const Vec2& other) = delete;
	friend std::ostream& operator<<(std::ostream& os, const Vec2& self) {
		os << "(" << self.x << "," << self.y << ")";
		return os;
	}

	int x, y;
};

bool Vec2::show_print = true;

struct NoDefaultVec2 : public Vec2
{
	NoDefaultVec2(int x, int y)
		: Vec2(x, y)
	{ }
};

struct NonCopyVec2 : public Vec2, public NonCopyStruct
{
	NonCopyVec2()
		: Vec2()
	{}

	NonCopyVec2(int x, int y)
		: Vec2(x, y)
	{}
};

namespace 
{
	struct data_set
	{
		data_set(int num = 10, int seed = 10) 
		{
			seed = seed;
			srand(seed);
			order_v2_.resize(num);
			descend_v2_.resize(num);
			random_v2_.resize(num);
			//order().resize(num);
			//descend().resize(num);
			//random().resize(num);
			nested_order_v2_.resize(num);
			nested_descend_v2_.resize(num);
			for (auto i = 0; i < num; ++i)
			{
				order_.emplace_back(i, i);
				descend_.emplace_back(num - i, num - i);
				auto x = rand() % num;
				auto y = rand() % num;
				random_.emplace_back(x,y );

				order_v2_[i].set(i);
				descend_v2_[i].set(num - i);
				random_v2_[i].set(x, y);

				std::list<NonCopyVec2> p;
				std::list<NonCopyVec2> d;
				auto& p1 = nested_order_v2_[i];
				auto& d1 = nested_descend_v2_[i];
				p1.resize(num);
				d1.resize(num);
				for (auto j = 0; j < num; ++j)
				{
					auto x = num * i + j;
					auto y = num * (num - 1 - i) + num - 1 - j;
					p.emplace_back(x, x);
					d.emplace_back(y, y);

					p1[j].set(x);
					d1[j].set(y);
				}

				nested_order_nc_.emplace_back(std::move(p));
				nested_descend_nc_.emplace_back(std::move(d));
				
			}


		}

		auto& const_order_noncopy() const {
			return order_;
		}

		auto& order_noncopy() {
			return order_;
		}

		auto& const_descend_noncopy() const{
			return descend_;
		}
		auto& descend_noncopy() {
			return descend_;
		}
		auto& const_random() const {
			return random_;
		}
		auto& random() {
			return random_;
		}
		auto& const_order_v2() const {
			return order_v2_;
		}

		auto& order_v2() {
			return order_v2_;
		}

		auto& const_descend_v2() const{
			return descend_v2_;
		}
		auto& descend_v2() {
			return descend_v2_;
		}

		auto& const_random_v2() const {
			return random_v2_;
		}
		auto& random_v2() {
			return random_v2_;
		}

		const auto& const_nested_order_noncopy() const {
			return nested_order_nc_;
		}

		const auto& const_nested_descend_noncopy() const {
			return nested_descend_nc_;
		}
		auto& nested_descend_noncopy() {
			return nested_descend_nc_;
		}

		auto& nested_order_noncopy() {
			return nested_order_nc_;
		}
		
		const auto& const_nested_order_v2() const {
			return nested_order_v2_;
		}

		auto& nested_order_v2() {
			return nested_order_v2_;
		}


		const auto& const_nested_descend_v2() const {
			return nested_descend_v2_;
		}

		auto& nested_descend_v2() {
			return nested_descend_v2_;
		}
	private:
		std::list<NonCopyVec2> order_;
		std::list<NonCopyVec2> descend_;
		std::list<NonCopyVec2> random_;

		std::vector<Vec2> order_v2_;
		std::vector<Vec2> descend_v2_;
		std::vector<Vec2> random_v2_;

		
		std::list< std::list<NonCopyVec2> > nested_order_nc_;
		std::list< std::list<NonCopyVec2> > nested_descend_nc_;
		vector<vector<Vec2>> nested_order_v2_;
		vector<vector<Vec2>> nested_descend_v2_;
		int seed = 10;
	};

	template<class T>
	auto& first(T&& arr) {
		return *std::begin(arr);
	}

	template<class T>
	auto& last(T&& arr) {
		return *std::rbegin(arr);
	}
}

void to_array_test()
{
	
}

void iterator_test()
{
	Vec2 hoge[2] = { {0, 0}, {1, 1} };
	// linq::Range(0, 10)
	 //.Where([](int a){ return (a%2) == 0;})
	 //.Select([](int a) { return Vec2{a, a};})
	 //.ToArray(hoge);
	auto v = linq::AsEnumerable(hoge);// .ToVector();
	int int_array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	auto index = 0;
	for (auto x : linq::AsEnumerable(int_array).Select([](int x) { return Vec2(x, x); }))
	{
		int i = int_array[index++];
		assert( Vec2(i, i)  == x);
	}

	index = 0;
	Vec2 hage[2] = { {0, 0}, {1, 1} };
	for (auto& r : linq::AsEnumerable(hoge))
	{
		assert(hage[index++] == r);
	}

	index = 0;
	for (auto x : std::get<0>(linq::AsEnumerable(int_array).ToArray<5>()))
	{
		int i = int_array[index++];
		assert(i == x);
	}
	assert(index == 5);

	int tmp[5];
	auto n = linq::AsEnumerable(int_array).ToArray(tmp);
	for (auto i : linq::Range(n)) {
		assert(tmp[i] == int_array[i]);
	}

	index = 0;
	for (auto x : linq::Range(0, 10).OrderByDescending<5>([](int a) { return a; }))
	{
		assert(x == (9 - index++));
	}
}

void any_test()
{
	Vec2 hoge[2] = { {2, 2}, {5, 5} };

	auto has_list = linq::AsEnumerable(hoge);
	auto emp_list = linq::Empty<int>();
	for (auto i = 0; i < 10; ++i) 
	{
		assert(has_list.Any());
		assert(!emp_list.Any());
	}

	data_set data;
	assert(!linq::AsEnumerable(data.order_noncopy()).Any([](NonCopyVec2& t) { return t.x > 100; }));

	assert(
		linq::AsEnumerable(data.nested_order_noncopy())
		.Where([](auto& v) { return linq::AsEnumerable(v).Any([](auto& x) { return x.x > 100; }); }).Any() == false);
}

void min_test()
{
	Vec2 hoge[] = { {8, 8}, {2, 2}, {5, 5} };

	assert(linq::AsEnumerable(hoge).Min([](const Vec2& x) {return x.x; }) == Vec2(2, 2));
	int int_hoge[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int int_hoge2[10] = { 9,8,7,6,5,4,3,2,1,0 };
	assert(linq::AsEnumerable(int_hoge).Min() == *std::min_element(std::begin(int_hoge), std::end(int_hoge)));
	assert(linq::AsEnumerable(int_hoge2).Min() == *std::min_element(std::begin(int_hoge2), std::end(int_hoge2)));

	data_set data;
	NonCopyVec2* tmp = nullptr;
	assert(linq::AsEnumerable(data.order_noncopy())
		.Select([](NonCopyVec2& x) {return &x; })
		.TryMin(tmp) && tmp);
}

void max_test()
{
	Vec2 hoge[] = { {8, 8}, {2, 2}, {5, 5} };
	assert(linq::AsEnumerable(hoge).Max([](const Vec2& x) {return x.x; }) == Vec2(8, 8));
	int int_hoge[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int int_hoge2[10] = { 9,8,7,6,5,4,3,2,1,0 };
	assert(linq::AsEnumerable(int_hoge).Max() == *std::max_element(std::begin(int_hoge), std::end(int_hoge)));
	assert(linq::AsEnumerable(int_hoge2).Max() == *std::max_element(std::begin(int_hoge2), std::end(int_hoge2)));
}

void skip_test()
{
	auto range = linq::Range(10).Skip(5);

	auto i = 5;
	while (range.Next()) {
		assert(range.Current() == (i++));
	}
}

void skip_while_test()
{
	int source[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	auto range = linq::AsEnumerable(source)
	.SkipWhile([](int x) { return x < 5; });
	assert(range.First() == 5);
}

void take_test()
{
	auto range = linq::Range(10).Take(5);
	auto i = 0;
	for (auto i = 0; i < 5; ++i) {
		assert(range.Next());
	}
	assert(!range.Next());

	i = 0;
	for (auto x : linq::Range(10).TakeUntil([](int x) {return x < 6; }))
	{
		assert(x == i++ && x < 6);
	}
} 

void element_at_test()
{
	auto range = linq::Range(10);
	for (auto i = 0; i < 10; ++i) {
		assert(range.ElementAt(i) == i);

		int x;
		assert(range.TryElementAt(i, x) && x == i);
	}

	int x;
	assert(range.TryElementAt(10, x) == false);

	// 範囲外の場合に型の規定値を返す
	assert(range.ElementAtOrDefault(10) == 0);
	// 範囲外の場合に引数で渡した値を返す
	assert(range.ElementAtOrDefault(10, 100) == 100);
}

void index_of_test()
{
	auto range = linq::Range(10);
	for (auto i = 0; i < 10; ++i) {
		assert(range.IndexOfV(i) == i);
	}

	assert(range.IndexOfV(100) == -1);

	assert(linq::Range(0).IndexOfV(0) == -1);
}

void first_test()
{
	Vec2 hoge[2] = { {2, 2}, {5, 5} };
	auto hoge_list = linq::AsEnumerable(hoge);
	assert(hoge[0].x == hoge_list.Select([](const Vec2& a) { return a.x; }).First());
	assert(Vec2() == linq::AsEnumerable(hoge).Take(0).FirstOrDefault());

	NoDefaultVec2 hage[2] = { {2, 2}, {5, 5} };
	assert(linq::AsEnumerable(hage).Take(0).FirstOrDefault(NoDefaultVec2(1, 1)) == NoDefaultVec2(1, 1));
	
	data_set data;
	assert(linq::AsEnumerable(data.order_noncopy()).First() == first(data.order_noncopy()));

	// x要素が奇数の最初の要素
	assert(linq::AsEnumerable(data.order_noncopy()).First([](NonCopyVec2& x) { return x.x % 2 == 1; }) == NonCopyVec2(1, 1));

	Vec2 out;
	assert(linq::AsEnumerable(data.order_v2()).Skip(2).TryFirst(out) && out == Vec2(2, 2));
	assert(linq::AsEnumerable(data.const_order_v2()).Take(0).TryFirst(out) == false);

	auto& elem = hoge_list.First([](const Vec2& v) {
		return v.x == 5;
		});
	assert(elem == Vec2(5, 5));

	assert( linq::AsEnumerable(data.order_noncopy())
		.Select([](NonCopyVec2& v) { return &v; })
		.FirstOrDefault([](NonCopyVec2* v) {
		return v == nullptr;
			}) == nullptr);
}

void remove_ref_test()
{
	/*data_set data;
	assert(linq::AsEnumerable(data.order_v2()).RemoveRef().First() == first(data.order_v2()));

	for (auto x : linq::AsEnumerable(data.order_v2()).RemoveRef())	{

	}*/
}

void order_by_test()
{

	for (auto i = 0; i < 10; ++i)
	{
		Vec2::show_print = false;
		data_set data;
		std::random_shuffle(data.order_v2().begin(), data.order_v2().end());
		Vec2::show_print = true;

		int index = 0;
		for (auto& x : linq::AsEnumerable(data.const_order_v2()).OrderBy<10>())
		{
			assert(x == Vec2(index, index));
			index++;
		}
	}
}

void order_by_descending_test()
{

	for (auto i = 0; i < 10; ++i)
	{
		Vec2::show_print = false;
		data_set data;
		std::random_shuffle(data.order_v2().begin(), data.order_v2().end());
		Vec2::show_print = true;

		int index = 0;
		for (auto& x : linq::AsEnumerable(data.const_order_v2()).OrderByDescending<10>())
		{
			assert(x == Vec2(9 - index, 9 - index));
			index++;
		}
	}
}

void last_test()
{
	data_set data;
	
	assert(linq::AsEnumerable(data.order_noncopy()).Last() == last(data.order_noncopy()));

	// コピーコンストラクタがないのでコンパイルエラーになれば正解
	//assert(linq::AsEnumerable(data.order()).Take(0).LastOrDefault() == NonCopyVec2());
	//assert(linq::AsEnumerable(data.order()).Take(0).LastOrDefault(NonCopyVec2(1, 1)) == NonCopyVec2(1, 1));
	assert(linq::AsEnumerable(data.order_v2()).Take(0).LastOrDefault() == Vec2());
	assert(linq::AsEnumerable(data.order_v2()).Take(0).LastOrDefault(Vec2(10, 10)) == Vec2(10, 10));

	Vec2 out;
	assert(linq::AsEnumerable(data.order_v2()).TryLast(out) && out == last(data.order_v2()));
	assert(linq::AsEnumerable(data.order_v2()).Take(0).TryLast(out) == false);
}

void sum_test()
{
	assert(linq::Range(10).Sum() == 45);
	data_set data;
	assert(linq::AsEnumerable(data.order_noncopy()).Sum([](NonCopyVec2& v) {
		return v.x;
		}) == 45);
	int num = 0;
	assert(linq::AsEnumerable(data.order_noncopy()).Sum([](NonCopyVec2& v) {
		return v.x;
		}, num) == 45 && num == 10);
}

void average_test()
{
	assert(linq::Range(10).Average() == 4);
	data_set data;
	assert(linq::AsEnumerable(data.order_noncopy()).Average([](NonCopyVec2& v) {
		return v.x;
		}) == 4);
	int num = 0;
	assert(linq::AsEnumerable(data.order_noncopy()).Average([](NonCopyVec2& v) {
		return v.x;
		}, num) == 4 && num == 10);
}

void aggregate_test()
{
	auto hoge = [](int sum, int n) { return sum + n; };
	using type = decltype(hoge);

	using arg = typename linq::lambda_type<type>::arg_type<0>;
	arg x = 0;
	//linq::lambda_arg_type_t<type, 0> x = 0;

	assert(linq::Range(10).Aggregate([](const int& sum, const int& n) { return sum + n; }) == 45);
	assert(linq::Range(10).Aggregate([](int sum, int n) { return sum + n; }, 10) == 55);
	assert(linq::Range(1, 5).Aggregate([](int x, int n) { return x * n; }, 1) == 120);
}

void count_test()
{
	assert(linq::Range(0, 10).Count() == 10);
	assert(linq::Range(0, 10).Count([](int x) { return (x % 2) == 0; }) == 5);
}

void empty_test()
{
	assert(linq::Empty<int>().Next() == false);
	assert(linq::Empty<int>().Count() == 0);
}

void all_test()
{
	assert( linq::Range(10).All([](int x) { return x < 100; } ));
	assert( linq::Range(10).All([](int x) { return x < 10; }));
	assert(!linq::Range(10).All([](int x) { return x < 9;  }));
	assert( linq::Range(10).All([](int x) { return x >= 0;  }));
}

void repeat_test()
{
	assert(linq::Repeat(10, 10).All([](int x) { return x == 10; }));
	assert(linq::Repeat(10, 10).Count() == 10);
}

void from_test()
{
	int hoge[] = { 1, 2, 3 };
	auto it = linq::AsEnumerable(hoge).Select([](const int &x) { return 2 * x; });
	assert(it.All([](int x) { return x % 2 == 0; }));
	data_set data;
	auto list = linq::AsEnumerable(data.descend_noncopy());
	int index = 0;
	while (list.Next()) {
		list.Current().x = index++;
	}
	index = 0;
	for (auto& x : linq::AsEnumerable(data.descend_noncopy()))
	{
		assert(x.x == index++);
	}

}

void concat_test()
{
	int head[] = { 0, 1, 2, 3, 4, 5 };
	int tail[] = { 6, 7, 8, 9 };
	auto list = linq::AsEnumerable(head).Concat(linq::AsEnumerable(tail));
	assert(list.Count() == 10);
	auto index = 0;
	for (auto a : list) {
		assert(index++ == a);
	}
}
void select_many_test()
{
	int hoge[][3] = { {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {9, 10, 11} };
	
	int i = 0;
	for (auto x : linq::AsEnumerable(hoge).SelectMany([](int x[3]) { return linq::AsEnumerable(x, 3); })) 
	{
		assert(x == i++);
	}
}

int main()
{
	struct elem {

		const char* label;
		void(*func)();
	};

	auto test = []( std::initializer_list<elem> elems ) 
	{
		for (auto e : elems) 
		{
			printf("%*s : ", 10, e.label);
			(*e.func)();
			cout << endl;
		}
	};
	test(
		{
		{"empty", empty_test},
		{"any", any_test },
		{"all", all_test},
		{"skip", skip_test},
		{"take", take_test},
		{"first", first_test},
		{"last", last_test},
		{"element_at", element_at_test },
		{"index_of", index_of_test },
		{"count", count_test},
		{"repeat", repeat_test},
		{"from", from_test},
		{"min", min_test},
		{"max", max_test},
		{"iterator", iterator_test},
		{"select_many", select_many_test},
		{"concat", concat_test},
		{"sum", sum_test},
		{"average", average_test},
		{"aggregate", aggregate_test},
		{"remove_ref", remove_ref_test},
		{"order_by", order_by_test},
		{"order_by_descending", order_by_descending_test},
		//{"average", average_test}
		}
		);
	//from_test();
	//select_many_test();
	auto x = 0;
	cout << "success" << endl;
	cin >> x;
}