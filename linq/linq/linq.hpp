﻿#pragma once
#include "linq_impl.hpp"
namespace linq
{
	template<class TSource>
	class EnumerableOf
	{
		template<class T>
		inline static EnumerableOf<T> As(T&& t) {
			return EnumerableOf<T>(std::forward<T>(t));
		}
	public:
		using source_type = raw_type_t<TSource>;
		using value_type = val_type_t<TSource>;
		using raw_value_type = raw_type_t<value_type>;
	public:
		EnumerableOf(source_type source)
			: source_(source)
		{}
	public:
		value_type Current() const
		{
			return source_.Current();
		}

		bool Next()
		{
			return source_.Next();
		}
	public:
		inline auto ToVector()
		{
			return ToVectorBuilder<source_type>().Build(*(source_type*)(this));
		}

		// -------------------------
		// ToArray
		// -------------------------
		// raw_value_typeがabstractクラスの場合に
		// clanでエラーになるので, テンプレート引数TValueを経由している
		template <int N, class TValue = raw_value_type>
		inline auto ToArray(TValue(&ret)[N]) -> decltype(auto)
		{
			return ToArrayBuilder<source_type>(). template Build<N>(source_, ret);
		}

		template<int N>
		inline auto ToArray()
		{
			return ToArrayBuilder<source_type>(). template Build<N>(source_);
		}
	public:
		inline auto begin() {
			return EnumerableIterator<source_type>(source_);
		}

		inline auto end() {
			return EnumerableIterator<source_type>();
		}
	public:
		// -------------------------
		// Where
		// -------------------------
		template <typename TPredicate>
		inline auto Where(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return As(WhereEnumerable<source_type, TPredicate>(source_, predicate));
		}
	public:
		// -------------------------
		// Skip
		// -------------------------
		inline auto Skip(int count)
		{
			return As(SkipEnumerable<source_type>(source_, count));
		}

		template<class TPredicate>
		inline auto SkipWhile(TPredicate predicate, decltype(is_executable<TPredicate, value_type>::value)* _ = nullptr)
		{
			//static_assert(is_executable<TPredicate, value_type>::value, "See define ofIEnumerable::SkipWhile Method");
			return As(SkipWhileEnumerable<source_type, TPredicate>(source_, predicate));
		}
	public:
		// -------------------------
		// Take
		// -------------------------
		inline auto Take(int count)
		{
			return As(TakeEnumerable<source_type>(source_, count));
		}

		template<class TPredicate>
		inline auto TakeUntil(TPredicate predicate)
		{
			return As(TakeUntilEnumerable<source_type, TPredicate>(source_, predicate));
		}
	public:
		// -------------------------
		// Select
		// -------------------------
		template <typename TPredicate>
		inline auto Select(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			static_assert(is_executable<TPredicate, value_type>::value, "See define ofIEnumerable::Select Method");
			return As(SelectEnumerable<source_type, TPredicate>(source_, predicate));
		}
	public:
		// -------------------------
		// Concat
		// -------------------------
		template <typename TTail>
		inline auto Concat(TTail tail_source)
		{
			using tail_type = raw_type_t<TTail>;
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return As(ConcatEnumerable<source_type, tail_type>(source_, tail_source));
		}
	public:
		// -------------------------
		// SelectMany
		// -------------------------
		template<class TCollectionSelector>
		inline auto SelectMany(TCollectionSelector selector)
		{
			return As(SelectManyEnumerable<source_type, TCollectionSelector>(source_, selector));
		}

		inline auto SelectMany()
		{
			auto selector = [](value_type v) { return AsEnumerable(v); };
			return As(SelectManyEnumerable<source_type, decltype(selector)>(source_, selector));
		}
	public:

		template<int N, typename TPredicate>
		inline auto OrderBy(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return As(OrderByEnumerable<source_type, TPredicate, N>(source_, predicate, true));
		}

		template<s32 N>
		inline auto OrderBy() -> decltype(auto)
		{
			return OrderBy<N>([](value_type v) -> const value_type{ return v; });
		}

		template<int N, typename TPredicate>
		inline auto OrderByDescending(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return As(OrderByEnumerable<source_type, TPredicate, N>(source_, predicate, false));
		}

		template<s32 N>
		inline auto OrderByDescending()
		{
			return OrderByDescending<N>([](value_type v) -> const value_type { return v; });
		}
	public:
		// -------------------------
		// First
		// -------------------------
		// value_typeがconstついているときのこと考えてraw_value_typeを使う
		inline auto TryFirst(raw_value_type& out)
		{
			return FirstBuilder<source_type>().TryBuild(source_, out);
		}

		// 参照が返る場合もあるのでdecltype(auto)にする
		inline auto First() -> decltype(auto)
		{
			return FirstBuilder<source_type>().Build(source_);
		}
		template<class TPredicate>
		inline auto First(TPredicate predicate) -> decltype(auto)
		{
			return SkipWhile([&](value_type v) {return !predicate(v); })
				.First();
		}

		// source_type::value_typeはこの段階では不完全だ他の為使えない為
		// const raw_value_type&を挟むことで引数無しで呼び出した場合にデフォルト値を扱えるようにしている
		inline auto FirstOrDefault(const raw_value_type& default_value = raw_value_type())
		{
			return FirstBuilder<source_type>().BuildOrDefault(source_, default_value);
		}

		template<class TPredicate>
		inline auto FirstOrDefault(TPredicate predicate, const raw_value_type& seed = raw_value_type())
		{
			return SkipWhile([&](value_type v) {return !predicate(v); })
				.FirstOrDefault(seed);
		}

	public:
		// -------------------------
		// Last
		// -------------------------
		inline auto TryLast(raw_value_type& out)
		{
			return LastBuilder<source_type>().TryBuild(source_, out);
		}

		// 参照が返る場合もあるのでdecltype(auto)にする
		inline auto Last() -> decltype(auto)
		{
			return LastBuilder<source_type>().Build(source_);
		}

		// source_type::value_typeはこの段階では不完全の為使えない為
		// const raw_value_type&を挟むことで引数無しで呼び出した場合にデフォルト値を扱えるようにしている
		inline auto LastOrDefault(const raw_value_type& default_value = raw_value_type())
		{
			return LastBuilder<source_type>().BuildOrDefault(source_, default_value);
		}

	public:
		// -------------------------
		// ElementAt
		// -------------------------
		inline auto TryElementAt(int index, raw_value_type& out)
		{
			return ElementAtBuilder<source_type>().TryBuild(source_, index, out);
		}

		inline auto ElementAt(int index) -> decltype(auto)
		{
			return ElementAtBuilder<source_type>().Build(source_, index);
		}

		// source_type::value_typeはこの段階では不完全だ他の為使えない為
		// const raw_value_type&を挟むことで引数無しで呼び出した場合にデフォルト値を扱えるようにしている
		inline auto ElementAtOrDefault(int index, const raw_value_type& default_value = raw_value_type())
		{
			return ElementAtBuilder<source_type>().BuildOrDefault(source_, index, default_value);
		}

	public:
		// -------------------------
		// Min
		// -------------------------
		template<class TSelector>
		inline auto Min(TSelector selector) -> decltype(auto)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
				// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			using select_value = decltype(selector(std::declval<value_type>()));
			return ComparerBuilder<source_type>().Build(source_, [](const select_value& a, const select_value& b) { return a < b; }, selector);
		}

		inline auto Min() -> decltype(auto)
		{
			using value_type = val_type_t<source_type>;
			return Min([](value_type t) -> value_type { return t; });
		}

		template<class TSelector>
		inline bool TryMin(TSelector selector, raw_value_type& out)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			using select_value = decltype(selector(std::declval<value_type>()));
			return ComparerBuilder<source_type>().TryBuild(source_, [](const select_value& a, const select_value& b) { return a < b; }, selector, out);
		}

		inline bool TryMin(raw_value_type& out)
		{
			// value_typeだとvalue_typeがコピー不可の場合にエラー
			// value_type&だとvalue_typeが値型の時にエラーになる場合があるのでconst 参照
			return TryMin([](const value_type& v) { return v; }, out);
		}

	public:
		// -------------------------
		// Max
		// -------------------------
		template<class TSelector>
		inline auto Max(TSelector selector) -> decltype(auto)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
					// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			using value_type = val_type_t<source_type>;
			using select_value = decltype(selector(std::declval<value_type>()));
			return ComparerBuilder<source_type>().Build(source_, [](const select_value& a, const select_value& b) { return a > b; }, selector);
		}

		inline auto Max() -> decltype(auto)
		{
			using value_type = val_type_t<source_type>;
			return Max([](value_type t) -> value_type { return t; });
		}

		template<class TSelector>
		inline bool TryMax(TSelector selector, value_type& out)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			using TSelectValue = decltype(selector(std::declval<value_type>()));
			return ComparerBuilder<source_type>().TryBuild(source_, [](const TSelectValue& a, const TSelectValue& b) { return a > b; }, selector, out);
		}

		inline bool TryMax(value_type& out)
		{
			// value_typeだとvalue_typeがコピー不可の場合にエラー
			// value_type&だとvalue_typeが値型の時にエラーになる場合があるのでconst 参照
			return TryMax([](const value_type& v) { return v; }, out);
		}

	public:
		// -------------------------
		// Any
		// -------------------------
		// Linq::Any
		bool Any() {
			return AnyBuilder<source_type>().Build(source_);
		}

		template<class TPredicate>
		bool Any(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return AnyBuilder<source_type>().Build(source_, predicate);
		}
	public:
		// -------------------------
		// All
		// -------------------------
		// Linq::All
		template<class TPredicate>
		bool All(TPredicate predicate)
		{
			// NOTE : ユニバーサル参照で受け取ると, predicateの引数はコピー渡しかconst 参照渡しである必要がある為コピーで受け取る
			// (gccで non-const lvalue reference to type cannot bind to a temporary of type エラーが起きる)
			return AllBuilder<source_type>().Build(source_, predicate);
		}
	public:
		// -------------------------
		// Count
		// -------------------------
		auto Count() {
			return CountBuilder<source_type>().Build(source_);
		}

		template<class TPredicate>
		auto Count(TPredicate predicate) {
			return CountBuilder<source_type>().Build(source_, predicate);
		}

	public:
		// -------------------------
		// Distinct
		// -------------------------
	public:
		// -------------------------
		// Sum
		// -------------------------
		// 合計値を返す
		inline auto Sum() -> decltype(auto)
		{
			// 型を直接指定してしまうとraw_value_typeが配列の時に
			// たとえ呼び出していなくてもコンパイルエラーになるのでautoにする
			s32 num = 0;
			return SumBuilder<source_type>().Build(source_, num);
		}

		// 合計値を返す
		inline auto Sum(s32& out_count)  -> decltype(auto) 
		{
			// 型を直接指定してしまうとraw_value_typeが配列の時に
			// たとえ呼び出していなくてもコンパイルエラーになるのでautoにする
			return SumBuilder<source_type>().Build(source_, out_count);
		}

		template<class TSelector>
		inline auto Sum(TSelector selector)  -> decltype(auto) 
		{
			s32 num = 0;
			return SumBuilder<source_type>().Build(source_, selector, num);
		}

		template<class TSelector>
		inline auto Sum(TSelector selector, s32& out_count)  -> decltype(auto) 
		{
			return SumBuilder<source_type>().Build(source_, selector, out_count);
		}
	public:
		// -------------------------
		// Average
		// -------------------------
		// 平均値を返す
		inline auto Average() -> decltype(auto) 
		{
			s32 out_count;
			return Average(out_count);
		}

		inline auto Average(s32& out_count)-> decltype(auto)
		{
			auto ret = Sum(out_count);
			if (out_count == 0)
				return ret;
			return ret / out_count;
		}

		template<class TSelector>
		inline auto Average(TSelector selector) -> decltype(auto) 
		{
			s32 num = 0;
			return Average(selector, num);
		}

		template<class TSelector>
		inline auto Average(TSelector selector, s32& out_count)-> decltype(auto) 
		{
			auto ret = Sum(selector, out_count);
			if (out_count == 0)
				return ret;
			return ret / out_count;
		}

	public:
		// -------------------------
		// IndexOf
		// -------------------------
		// 要素の値を渡すタイプ
		template<class TValue = raw_value_type>
		inline auto IndexOfV(const TValue& val) -> decltype(auto)
		{
			return IndexOfBuilder<source_type>().Build(source_, [&](value_type v) { return v == val; });
		}
		// ラムダ式を渡すタイプ
		template<class TPredicate>
		inline auto IndexOf(TPredicate&& predicate) -> decltype(auto)
		{
			return IndexOfBuilder<source_type>().Build(source_, predicate);
		}
	public:
		// -------------------------
		// Aggregate
		// -------------------------
		template<class TAccum, class TValue = raw_type_t<lambda_arg_type_t<TAccum, 0>>>
		inline auto Aggregate(TAccum accumulator, const TValue& init_value = TValue()) -> decltype(auto)
		{
			return AggregateBuilder<source_type>().Build(source_, accumulator, init_value);
		}
	public:
		// -------------------------
		// Union
		// -------------------------
	public:
		// -------------------------
		// Except
		// -------------------------
	public:
		// -------------------------
		// Intersect
		// -------------------------
	public:
		// -------------------------
		// ThenBy
		// -------------------------
	public:
		// -------------------------
		// Reverse
		// -------------------------
	public:
		// -------------------------
		// GroupBy
		// -------------------------
	public:
		// -------------------------
		// Join
		// -------------------------
	public:
		// -------------------------
		// DefaultIfEmpty
		// -------------------------
	public:
		// -------------------------
		// Zip
		// -------------------------
	public:
		// -------------------------
		// OfType -- dynamic cast使えない状態だと無理
		// -------------------------
	public:
		// -------------------------
		// Cast
		// -------------------------
		template<class T>
		auto Cast() {
			return Select([](value_type v) -> T {
				return static_cast<T>(v); 
			});
		}
	public:
		// -------------------------
		// RemoveRef(c++限定, 参照型を値型に変換する)
		// -------------------------
		auto RemoveRef() -> decltype(auto)
		{
			using t = typename std::is_reference<value_type>::type;
			return RemoveRefImpl(t());
		}

	private:
		// もともと値型の場合はそのまま
		inline auto& RemoveRefImpl(std::false_type) {
			return *this;
		}
		// 参照型の場合 value_type -> std::remove_reference_t<value_type>に変換する
		// constを残すためにraw_value_typeではない(値型のconstは特に意味ないけど)
		inline auto RemoveRefImpl(std::true_type) {
			return Select([](value_type v) -> raw_value_type
				{
					return v;
				});
		}
	private:
		TSource source_;
	};
	namespace _impl
	{
		template<class T>
		inline static EnumerableOf<T> As(T&& t) {
			return EnumerableOf<T>(std::forward<T>(t));
		}
	}

	template<class TIterator>
	inline auto AsEnumerable(TIterator&& begin, TIterator&& end)
	{
		return  _impl::As(FromEnumerable<TIterator>(begin, end));
	}

	template<class TContainer>
	inline auto AsEnumerable(TContainer&& container)
	{
		return AsEnumerable(std::begin(container), std::end(container));
	}

	template<class TValue>
	inline auto AsEnumerable(TValue* ptr, size_type size) {
		return  _impl::As(FromEnumerable<TValue*>(ptr, ptr + size));
	}

	inline auto Range(int start, int count)
	{
		return  _impl::As(RangeEnumerable(start, count));
	}

	inline auto Range(int count)
	{
		return Range(0, count);
	}

	template<class TValue>
	inline auto Repeat(const TValue& value, size_type count)
	{
		return  _impl::As(RepeatEnumerable<TValue>(value, count));
	}

	template<class TValue>
	inline auto Empty()
	{
		return _impl::As(EmptyEnumerable<TValue>());
	}
}